package ll.lang


import ic.base.arrays.ext.forEach
import ic.base.objects.ext.asLlObject
import ic.struct.map.editable.EditableMap


fun Object (vararg mappings: Pair<String, Any?>) : Object {

	val fields = EditableMap<String, Object>()

	mappings.forEach { (key, value) ->
		fields[key] = value?.asLlObject
	}

	return FieldsObject(
		fields = fields
	)

}