package ll.lang.storage


import ic.base.strings.ext.parseLlObject
import ic.base.strings.ext.toByteSequence

import ic.storage.fs.Directory
import ic.storage.fs.File
import ic.storage.fs.ext.*
import ic.stream.sequence.ext.toString
import ic.struct.collection.ext.count.count
import ic.struct.map.editable.BaseEditableMap
import ic.util.text.charset.Charset.Companion.Utf8

import ll.lang.FieldsObject
import ll.lang.Object


internal abstract class LlStorageFromDirectory : BaseEditableMap<String, Object>() {


	protected abstract val directory : Directory


	override val keys get() = directory.getItemsNames()

	override val count get() = directory.getItemsNames().count


	override fun empty() {
		directory.empty()
	}


	override fun get (key: String) : Object? {
		val childFile = directory.getItemOrNull(key + ".ll")
		if (childFile is File) {
			return childFile.read().toString(charset = Utf8).parseLlObject
		}
		val childDirectory = directory.getItemOrNull(key)
		if (childDirectory is Directory) {
			return FieldsObject(
				fields = childDirectory.asLlStorage
			)
		}
		else return null
	}


	override fun set (key: String, value: Object?) {
		if (value == null) {
			directory.removeIfExists(key + ".ll")
			directory.removeIfExists(key)
		} else {
			directory.writeFileReplacing(key + ".ll") {
				write(
					value.toString().toByteSequence(charset = Utf8)
				)
			}
		}
	}


}