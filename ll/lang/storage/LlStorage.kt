package ll.lang.storage


import ic.struct.map.editable.EditableMap

import ll.lang.Object


typealias LlStorage = EditableMap<String, Object>