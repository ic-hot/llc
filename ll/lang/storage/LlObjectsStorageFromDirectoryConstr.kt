package ll.lang.storage


import ic.storage.fs.Directory


internal fun LlObjectsStorageFromDirectory (

	directory : Directory

) : LlStorageFromDirectory {

	return object : LlStorageFromDirectory() {

		override val directory get() = directory

	}

}