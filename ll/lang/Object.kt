package ll.lang


import ic.base.primitives.int32.Int32
import ic.struct.map.finite.FiniteMap
import ic.text.TextBuffer
import ic.text.TextOutput

import ll.lang.value.Value


abstract class Object {


	internal abstract val value : Value


	internal abstract val shouldBeInvoked : Boolean

	abstract operator fun invoke (arg: Object) : Object


	abstract val fields : FiniteMap<String, Object>


	internal abstract fun TextOutput.write (indent: Int32, tabSize: Int32)

	final override fun toString() : String {
		return TextBuffer().apply {
			write(
				indent = -1,
				tabSize = 4
			)
		}.toString()
	}


}