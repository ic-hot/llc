package ll.lang.parse.statements


import ic.base.throwables.MessageException
import ic.struct.list.List
import ic.struct.list.ext.length.isEmpty

import ll.lang.code.Statement
import ll.lang.parse.lines.Line


internal abstract class ReadStatementsState {


	@Throws(MessageException::class)
	abstract fun addLine (line: Line) : NonEmpty


	@Throws(MessageException::class)
	fun onNewLine (line: Line) : ReadStatementsState {
		if (line.words.isEmpty) {
			return this
		} else {
			return addLine(line)
		}
	}


	abstract fun finish () : List<Statement>


}