package ll.lang.parse.statements


import ic.struct.list.List
import ic.struct.list.ext.copy.convert.copyConvert

import ll.lang.code.Expression
import ll.lang.parse.lines.Line


internal object Empty : ReadStatementsState() {

	override fun addLine (line: Line) = TopLevel(
		indent = line.indent,
		statements = List(),
		lastStatementExpressions = line.words.copyConvert { Expression.Word(it) },
		endsWithBlock = false
	)

	override fun finish() = List()

}