package ll.lang.parse.statements


import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException
import ic.struct.list.List
import ic.struct.list.ext.copy.convert.copyConvert

import ll.lang.code.Expression
import ll.lang.parse.lines.Line


internal abstract class NonEmpty : ReadStatementsState() {


	protected abstract val indent : Int32

	protected abstract val endsWithBlock : Boolean


	protected abstract fun addSameIndentLine (expressions: List<Expression>) : NonEmpty

	abstract fun addBlock (block: Expression.Block) : NonEmpty

	@Throws(MessageException::class)
	protected abstract fun onEndOfIndent (line: Line) : NonEmpty


	@Throws(MessageException::class)
	final override fun addLine (line: Line) = when {

		line.indent == indent -> addSameIndentLine(
			expressions = line.words.copyConvert { Expression.Word(it) }
		)

		line.indent > indent -> {
			if (endsWithBlock) throw RuntimeException("Inconsistent indentation")
			Indented(
				parent = this,
				indent = line.indent,
				statements = List(),
				lastStatementExpressions = line.words.copyConvert { Expression.Word(it) },
				endsWithBlock = false
			)
		}

		else -> onEndOfIndent(line = line)

	}


}