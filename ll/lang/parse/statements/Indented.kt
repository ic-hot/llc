package ll.lang.parse.statements


import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException
import ic.struct.list.List
import ic.struct.list.ext.plus

import ll.lang.code.Expression
import ll.lang.code.Statement
import ll.lang.parse.lines.Line


internal class Indented (

	private val parent : NonEmpty,

	override val indent : Int32,

	private val statements : List<Statement>,

	private val lastStatementExpressions : List<Expression>,

	override val endsWithBlock : Boolean

) : NonEmpty() {

	override fun addSameIndentLine (expressions: List<Expression>) = (
		Indented(
			parent = parent,
			indent = indent,
			statements = statements + Statement(lastStatementExpressions),
			lastStatementExpressions = expressions,
			endsWithBlock = false
		)
	)

	override fun addBlock (block: Expression.Block) = (
		Indented(
			parent = parent,
			indent = indent,
			statements = statements,
			lastStatementExpressions = lastStatementExpressions + block,
			endsWithBlock = true
		)
	)

	@Throws(MessageException::class)
	override fun onEndOfIndent (line: Line) = (
		parent.addBlock(
			block = Expression.Block(
				statements = statements + Statement(lastStatementExpressions)
			)
		)
		.addLine(line)
	)

	override fun finish() = (
		parent.addBlock(
			block = Expression.Block(
				statements = statements + Statement(lastStatementExpressions)
			)
		)
		.finish()
	)

}