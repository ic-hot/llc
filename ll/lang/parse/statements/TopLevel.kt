package ll.lang.parse.statements


import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException
import ic.struct.list.List
import ic.struct.list.ext.plus

import ll.lang.code.Expression
import ll.lang.code.Statement
import ll.lang.parse.lines.Line


internal class TopLevel (

	override val indent : Int32,

	private val statements : List<Statement>,

	private val lastStatementExpressions : List<Expression>,

	override val endsWithBlock: Boolean

) : NonEmpty() {

	override fun addSameIndentLine (expressions: List<Expression>) = (
		TopLevel(
			indent = indent,
			statements = statements + Statement(lastStatementExpressions),
			lastStatementExpressions = expressions,
			endsWithBlock = false
		)
	)

	override fun addBlock (block: Expression.Block) = (
		TopLevel(
			indent = indent,
			statements = statements,
			lastStatementExpressions = lastStatementExpressions + block,
			endsWithBlock = true
		)
	)

	@Throws(MessageException::class)
	override fun onEndOfIndent (line: Line) = (
		throw MessageException("Inconsistent indentation")
	)

	override fun finish() = (
		statements + Statement(lastStatementExpressions)
	)

}