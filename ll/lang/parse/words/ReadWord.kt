package ll.lang.parse.words


import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.End
import ic.base.throwables.MessageException
import ic.text.input.TextInput


internal data class ReadWordResult (
	val indent : Int32,
	val word : String?,
	val status : Status
) {
	sealed class Status {
		data object EndOfWord : Status()
		data object EndOfLine : Status()
		data object EndOfFile : Status()
	}
}

@Throws(MessageException::class)
internal fun TextInput.readWord (tabSize: Int32) : ReadWordResult {

	var state : ReadWordState = Empty(
		indent = 0,
		tabSize = tabSize
	)

	loop {

		val character = try {

			getNextCharacterOrThrowEnd()

		} catch (_: End) {

			return ReadWordResult(
				indent = state.indent,
				word = state.finish(),
				status = ReadWordResult.Status.EndOfFile
			)

		}

		try {

			state = state.onNextCharacter(character)

		} catch (_: ReadWordState.EndOfWord) {

			return ReadWordResult(
				indent = state.indent,
				word = state.finish(),
				status = ReadWordResult.Status.EndOfWord
			)

		} catch (_: ReadWordState.EndOfLine) {

			return ReadWordResult(
				indent = state.indent,
				word = state.finish(),
				status = ReadWordResult.Status.EndOfLine
			)

		}

	}

}


