package ll.lang.parse.words


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException


internal class Quote (

	override val indent : Int32,

	private val word : String

) : ReadWordState {


	override fun onNextCharacter (character: Character) = when (character) {

		'"' -> NormalWord(
			indent = indent,
			word = word + character
		)

		else -> Quote(
			indent = indent,
			word = word + character
		)

	}


	@Throws(MessageException::class)
	override fun finish() = throw MessageException(
		"Unexpected end of file in a string literal"
	)


}