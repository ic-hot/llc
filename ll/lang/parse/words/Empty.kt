package ll.lang.parse.words


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32

import ll.lang.parse.words.ReadWordState.EndOfLine
import ll.lang.parse.words.ReadWordState.EndOfWord


internal class Empty (

	override val indent : Int32,

	private val tabSize : Int32

) : ReadWordState {


	@Throws(EndOfWord::class, EndOfLine::class)
	override fun onNextCharacter (character: Character) = when (character) {

		' ' -> Empty(
			indent = indent + 1,
			tabSize = tabSize
		)

		'\t' -> Empty(
			indent = indent / tabSize * tabSize + tabSize,
			tabSize = tabSize
		)

		'\n' -> throw EndOfLine

		'"' -> Quote(
			indent = indent,
			word = "\""
		)

		'/' -> Slash(
			indent = indent
		)

		else -> NormalWord(
			indent = indent,
			word = character.toString()
		)

	}


	override fun finish() = null


}