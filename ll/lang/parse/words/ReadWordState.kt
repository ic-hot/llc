package ll.lang.parse.words


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException


internal interface ReadWordState {


	val indent : Int32


	object EndOfWord : Exception()

	object EndOfLine : Exception()

	@Throws(EndOfWord::class, EndOfLine::class, MessageException::class)
	fun onNextCharacter (character: Character) : ReadWordState


	@Throws(MessageException::class)
	fun finish () : String?


}