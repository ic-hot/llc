package ll.lang.parse.words


import ic.base.primitives.character.Character

import ll.lang.parse.words.ReadWordState.EndOfLine


internal object Comment : ReadWordState {

	override val indent get() = 0

	override fun onNextCharacter (character: Character) = when (character) {

		'\n' -> throw EndOfLine

		else -> Comment

	}

	override fun finish() = null

}