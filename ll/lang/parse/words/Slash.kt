package ll.lang.parse.words


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException

import ll.lang.parse.words.ReadWordState.EndOfLine
import ll.lang.parse.words.ReadWordState.EndOfWord


internal class Slash (

	override val indent : Int32

) : ReadWordState {

	@Throws(EndOfWord::class, EndOfLine::class, MessageException::class)
	override fun onNextCharacter (character: Character) = when (character) {

		'/' -> Comment

		' ', '\t' -> throw EndOfWord

		'\n' -> throw EndOfLine

		else -> NormalWord(
			indent = indent,
			word = "/" + character
		)

	}

	override fun finish() = "/"

}