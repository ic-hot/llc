package ll.lang.parse.words


import ic.base.primitives.character.Character
import ic.base.primitives.int32.Int32

import ll.lang.parse.words.ReadWordState.EndOfLine
import ll.lang.parse.words.ReadWordState.EndOfWord


internal class NormalWord (

	override val indent : Int32,

	private val word : String

) : ReadWordState {


	@Throws(EndOfWord::class, EndOfLine::class)
	override fun onNextCharacter (character: Character) = when (character) {

		' ', '\t' -> throw EndOfWord

		'\n' -> throw EndOfLine

		else -> NormalWord(
			indent = indent,
			word = word + character
		)

	}


	override fun finish() = word


}