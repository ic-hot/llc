package ll.lang.parse.lines


import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.length.isEmpty
import ic.text.input.TextInput

import ll.lang.parse.words.ReadWordResult
import ll.lang.parse.words.readWord


internal data class ReadLineResult (
	val line : Line?,
	val status : Status
) {
	sealed class Status {
		data object EndOfLine : Status()
		data object EndOfFile : Status()
	}
}

@Throws(MessageException::class)
internal fun TextInput.readLine (tabSize: Int32) : ReadLineResult {

	var indent : Int32? = null

	val words = EditableList<String>()

	loop {

		val readWordResult = readWord(tabSize = tabSize)

		if (indent == null) indent = readWordResult.indent

		readWordResult.word?.let { words.add(it) }

		when (readWordResult.status) {

			is ReadWordResult.Status.EndOfWord -> {}

			is ReadWordResult.Status.EndOfLine -> {
				val line = (
					if (words.isEmpty) {
						null
					} else {
						Line(
							indent = indent!!,
							words = words
						)
					}
				)
				return ReadLineResult(
					line = line,
					status = ReadLineResult.Status.EndOfLine
				)
			}

			is ReadWordResult.Status.EndOfFile -> {
				val line = (
					if (words.isEmpty) {
						null
					} else {
						Line(
							indent = indent!!,
							words = words
						)
					}
				)
				return ReadLineResult(
					line = line,
					status = ReadLineResult.Status.EndOfFile
				)
			}

		}

	}

}


