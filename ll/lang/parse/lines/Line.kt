package ll.lang.parse.lines


import ic.base.primitives.int32.Int32
import ic.struct.list.List


data class Line (

	val indent : Int32,

	val words : List<String>

)