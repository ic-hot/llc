package ll.lang.parse


import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException
import ic.text.input.TextInput

import ll.lang.Object
import ll.lang.code.ext.evaluateBlock


@Throws(MessageException::class)
fun TextInput.readLlObject (

	tabSize : Int32 = 4

) : Object {

	val statements = readStatements(tabSize = tabSize)

	return evaluateBlock(statements)

}