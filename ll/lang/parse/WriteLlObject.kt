package ll.lang.parse


import ic.base.primitives.int32.Int32
import ic.text.TextOutput

import ll.lang.Object


fun TextOutput.writeLlObject (

	obj : Object,

	indent : Int32 = 0,
	tabSize : Int32 = 4

) {

	obj.run {
		write(
			indent = indent,
			tabSize = tabSize
		)
	}

}