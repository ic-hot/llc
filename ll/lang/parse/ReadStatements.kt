package ll.lang.parse


import ic.base.loop.loop
import ic.base.primitives.int32.Int32
import ic.base.throwables.MessageException
import ic.struct.list.List
import ic.text.input.TextInput

import ll.lang.code.Statement
import ll.lang.parse.lines.ReadLineResult
import ll.lang.parse.lines.readLine
import ll.lang.parse.statements.Empty
import ll.lang.parse.statements.ReadStatementsState


@Throws(MessageException::class)
internal fun TextInput.readStatements (

	tabSize : Int32

) : List<Statement> {

	var state : ReadStatementsState = Empty

	loop {

		val readLineResult = readLine(
			tabSize = tabSize
		)

		val line = readLineResult.line

		if (line != null) {
			state = state.onNewLine(line)
		}

		when (readLineResult.status) {
			ReadLineResult.Status.EndOfLine -> {}
			ReadLineResult.Status.EndOfFile -> {
				return state.finish()
			}
		}

	}

}