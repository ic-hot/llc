package ll.lang


import ic.base.primitives.int32.Int32
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.Empty
import ic.text.TextOutput

import ll.lang.value.ConcreteBoolean
import ll.lang.value.Value


internal data class ValueObject (

	override val value : Value

) : NonFunction() {


	override val fields get() = FiniteMap.Empty<String, Object>()


	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		value.run {
			write(
				indent = indent,
				tabSize = tabSize
			)
		}
	}


	companion object {

		val False = ValueObject(
			value = ConcreteBoolean.False
		)

		val True = ValueObject(
			value = ConcreteBoolean.True
		)

	}


}