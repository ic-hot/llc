package ll.lang.value


import ic.base.primitives.int32.Int32
import ic.text.TextOutput
import ic.util.code.clike.quote.doubleQuote


data class ConcreteString (

	val string : String

) : ConcreteType.String, ConcreteValue {


	override val primitiveValueOrNull get() = string


	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		write(
			doubleQuote(string)
		)
	}


}