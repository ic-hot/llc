package ll.lang.value


internal sealed interface ConcreteType : Value {


	interface Boolean : ConcreteType {

		override val primitiveValueOrNull : kotlin.Boolean?

	}


	interface Number : ConcreteType {

		override val primitiveValueOrNull : ic.math.numbers.Number?

	}


	interface String : ConcreteType {

		override val primitiveValueOrNull : kotlin.String?

	}


}