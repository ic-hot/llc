package ll.lang.value


internal fun mergeValues (a: Value, b: Value) : Value = when {

	a is Anything -> b
	b is Anything -> a

	a is Nothing -> Nothing
	b is Nothing -> Nothing

	else -> throw NotImplementedError()

}