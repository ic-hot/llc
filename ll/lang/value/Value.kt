package ll.lang.value


import ic.base.primitives.int32.Int32
import ic.text.TextOutput


internal interface Value {


	val primitiveValueOrNull : Any?


	fun TextOutput.write (indent: Int32, tabSize: Int32)


}