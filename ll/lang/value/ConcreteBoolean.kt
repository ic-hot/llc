package ll.lang.value


import ic.base.primitives.int32.Int32
import ic.text.TextOutput


sealed class ConcreteBoolean : ConcreteType.Boolean, ConcreteValue {


	abstract val boolean : Boolean


	override val primitiveValueOrNull get() = boolean


	object False : ConcreteBoolean() {

		override val boolean get() = false

		override fun TextOutput.write (indent: Int32, tabSize: Int32) {
			write("False")
		}

	}

	object True : ConcreteBoolean() {

		override val boolean get() = true

		override fun TextOutput.write (indent: Int32, tabSize: Int32) {
			write("True")
		}

	}


}