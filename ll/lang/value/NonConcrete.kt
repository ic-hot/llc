package ll.lang.value


internal interface NonConcrete : Value {


	override val primitiveValueOrNull get() = null


}