package ll.lang.value


fun ConcreteBoolean (

	primitive : Boolean

) : ConcreteBoolean {

	if (primitive) {

		return ConcreteBoolean.True

	} else {

		return ConcreteBoolean.False

	}

}