package ll.lang.value


import ic.base.primitives.int32.Int32
import ic.text.TextOutput


object Anything : NonConcrete {

	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		throw NotImplementedError()
	}

}