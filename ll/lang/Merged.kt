package ll.lang


import ic.base.primitives.int32.Int32
import ic.struct.map.finite.FiniteMap
import ic.text.TextOutput

import ll.lang.value.mergeValues


class Merged (

	private val a : Object,
	private val b : Object

) : Object() {


	override val value get() = mergeValues(a.value, b.value)


	override val shouldBeInvoked get() = a.shouldBeInvoked || b.shouldBeInvoked


	override fun invoke (arg: Object) : Object {
		TODO("Not yet implemented")
	}


	override val fields : FiniteMap<String, Object>
		get() = TODO("Not yet implemented")


	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		write("Merged")
	}


}