package ll.lang


import ic.base.loop.repeat
import ic.base.primitives.int32.Int32
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach
import ic.text.TextOutput

import ll.lang.value.Anything


internal class FieldsObject (

	override val fields : FiniteMap<String, Object>

) : NonFunction() {

	override val value get() = Anything

	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		fields.forEach { key, value ->
			writeNewLine()
			repeat(
				(indent + 1) * tabSize
			) {
				putCharacter(' ')
			}
			write(key)
			write(" ")
			value.run {
				write(
					indent = indent + 1,
					tabSize = tabSize
				)
			}
		}
	}

}