package ll.lang.ext


import ll.lang.Object


val Object?.asStringOrNull : String? get() {

	if (this == null) return null

	val value = this.value

	return value.primitiveValueOrNull as? String

}


val Object?.asString get() = asStringOrNull!!