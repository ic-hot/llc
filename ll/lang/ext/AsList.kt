package ll.lang.ext


import ic.struct.list.List

import ll.lang.ListObject
import ll.lang.Object


val Object?.asListOrNull : List<Object>? get() {

	if (this == null) return null

	when (this) {

		is ListObject -> return this.list

		else -> return null

	}

}


val Object?.asList get() = asListOrNull!!

val Object?.asListOrEmpty get() = asListOrNull ?: List()