package ll.lang.ext


import ll.lang.Object


operator fun Object?.get (key: String) : Object? {

	if (this == null) return null

	return fields[key]

}