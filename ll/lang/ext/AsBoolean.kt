package ll.lang.ext


import ll.lang.Object


val Object?.asBooleanOrNull : Boolean? get() {

	if (this == null) return null

	val value = this.value

	return value.primitiveValueOrNull as? Boolean

}


val Object?.asBoolean get() = asBooleanOrNull!!