package ll.lang.code


import ic.struct.list.List
import ic.struct.list.ext.copySubList


data class Statement (

	val head : String,

	val tail : List<Expression>

) {

	constructor (expressions: List<Expression>) : this (
		head = (expressions[0] as Expression.Word).word,
		tail = expressions.copySubList(1, expressions.length)
	)

}