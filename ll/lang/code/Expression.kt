package ll.lang.code


import ic.struct.list.List


sealed class Expression {

	data class Word (val word: String) : Expression()

	data class Block (val statements: List<Statement>) : Expression()

}