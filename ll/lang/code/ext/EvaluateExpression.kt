package ll.lang.code.ext


import ic.base.strings.ext.endsWith
import ic.base.strings.ext.startsWith
import ic.util.code.clike.quote.doubleUnquote
import ll.lang.ListObject

import ll.lang.Object
import ll.lang.ValueObject
import ll.lang.code.Expression
import ll.lang.value.ConcreteString


fun evaluate (expression: Expression) : Object = when (expression) {

	is Expression.Word -> {

		val word = expression.word

		when {

			word startsWith '"' && word endsWith '"' -> {
				val unquoted = doubleUnquote(word)
				ValueObject(
					value = ConcreteString(unquoted)
				)
			}

			word == "False" -> ValueObject.False
			word == "True"  -> ValueObject.True

			word == "list.Empty" -> ListObject.Empty

			else -> throw NotImplementedError()

		}

	}

	is Expression.Block -> evaluateBlock(expression.statements)

}