package ll.lang.code.ext


import ic.struct.list.List
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.reduce.find.all
import ic.struct.map.editable.EditableMap

import ll.lang.FieldsObject
import ll.lang.ListObject
import ll.lang.Object
import ll.lang.code.Statement


internal fun evaluateBlock (statements: List<Statement>) : Object {

	when {

		statements.all { it.head == "#" } -> {
			return ListObject(
				list = statements.copyConvert { evaluateTail(it) }
			)
		}

		else -> {
			val fields = EditableMap<String, Object>()
			statements.forEach { statement ->
				fields[statement.head] = evaluateTail(statement)
			}
			return FieldsObject(
				fields = fields
			)
		}

	}

}