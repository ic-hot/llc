package ll.lang.code.ext


import ic.struct.list.ext.foreach.forEach

import ll.lang.EmptyObject
import ll.lang.Object
import ll.lang.code.Statement


fun evaluateTail (statement: Statement) : Object {

	var result : Object = EmptyObject

	statement.tail.forEach { itemExpression ->

		val itemObject = evaluate(itemExpression)

		result = result(itemObject)

	}

	return result

}