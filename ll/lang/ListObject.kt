package ll.lang


import ic.base.loop.repeat
import ic.base.primitives.int32.Int32
import ic.struct.list.List
import ic.struct.list.ext.Empty
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.length.isEmpty
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.Empty
import ic.text.TextOutput

import ll.lang.value.Anything


internal data class ListObject (

	val list : List<Object>

) : NonFunction() {


	override val value get() = Anything


	override val fields get() = FiniteMap.Empty<String, Object>()


	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		if (list.isEmpty) {
			write("list.Empty")
		} else {
			list.forEach { item ->
				writeNewLine()
				repeat(
					(indent + 1) * tabSize
				) {
					putCharacter(' ')
				}
				write("# ")
				item.run {
					write(
						indent = indent + 1,
						tabSize = tabSize
					)
				}
			}
		}
	}


	companion object {

		val Empty = ListObject(List.Empty)

	}


}