package ll.lang


import ic.base.primitives.int32.Int32
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.Empty
import ic.text.TextOutput

import ll.lang.value.Anything


internal object EmptyObject : NonFunction() {


	override val value get() = Anything


	override val fields get() = FiniteMap.Empty<String, Object>()


	override fun TextOutput.write (indent: Int32, tabSize: Int32) {
		throw NotImplementedError()
	}


	override fun invoke (arg: Object) : Object {
		return arg
	}


}