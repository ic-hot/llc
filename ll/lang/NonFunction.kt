package ll.lang


internal abstract class NonFunction : Object() {


	final override val shouldBeInvoked get() = false


	override fun invoke (arg: Object) : Object = Merged(arg, this)


}