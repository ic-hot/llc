package ic.base.strings.ext


import ll.lang.Object
import ll.lang.parse.readLlObject


val String.parseLlObject : Object get() {

	return this.asText.newIterator().readLlObject()

}