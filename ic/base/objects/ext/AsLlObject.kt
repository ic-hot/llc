package ic.base.objects.ext


import ic.base.reflect.ext.className
import ic.struct.list.List
import ic.struct.list.cache.static.CacheConvertList

import ll.lang.EmptyObject
import ll.lang.ListObject
import ll.lang.Object
import ll.lang.ValueObject
import ll.lang.value.ConcreteBoolean
import ll.lang.value.ConcreteString


val Any.asLlObject : Object get() = when (this) {

	is Boolean -> ValueObject(
		value = ConcreteBoolean(this)
	)

	is String -> ValueObject(
		value = ConcreteString(this)
	)

	is Object -> this

	is List<*> -> ListObject(
		list = CacheConvertList(this) { it?.asLlObject ?: EmptyObject }
	)

	else -> throw RuntimeException("className: $className")

}