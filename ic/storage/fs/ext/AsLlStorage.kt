package ic.storage.fs.ext


import ic.storage.fs.Directory

import ll.lang.storage.LlStorage
import ll.lang.storage.LlObjectsStorageFromDirectory


val Directory.asLlStorage : LlStorage get() = LlObjectsStorageFromDirectory(this)